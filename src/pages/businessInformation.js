import React from "react"
import TextField from "@material-ui/core/TextField"
import produce from "immer"
import Box from "@material-ui/core/Box"

const MyTextField = ({ id, label, value, onChange, type }) => {
  const [error, setError] = React.useState()
  const fieldType = type ? type : "text"
  return (
    <div>
      <TextField
        required
        type={fieldType}
        fullWidth
        error={error}
        id={id}
        label={label}
        value={value}
        onChange={e => onChange(e.target.value)}
        onBlur={e => setError(e.target.value <= 0)}
      />
    </div>
  )
}

const BusinessInformation = ({ value, onChange }) => {
  const [state, setState] = React.useState(value)
  React.useEffect(() => {
    if (onChange) onChange(state)
  }, [state, onChange])
  if (!value) return <p>loading...</p>
  return (
    <Box mb={3}>
      <div>
        <MyTextField
          value={state.duns}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.duns = val))
            })
          }}
          id="duns"
          label="D & B DUNS Number"
        />
      </div>
      <div>
        <MyTextField
          value={state.certificateOfIncorporation}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.certificateOfIncorporation = val)
              )
            })
          }}
          id="certificateOfIncorporation"
          label="Certificate of Incorporation"
        />
      </div>
      <div>
        <MyTextField
          value={state.certificateOfInsurance}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.certificateOfInsurance = val)
              )
            })
          }}
          id="certificateOfInsurance"
          label="Cerfificate of Insurance"
        />
      </div>
      <div>
        <MyTextField
          value={state.website}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.website = val))
            })
          }}
          id="website"
          label="Business Website"
        />
      </div>
      <div>
        <MyTextField
          type="number"
          value={state.yearStarted}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.yearStarted = val))
            })
          }}
          id="yearStarted"
          label="Year Business Started"
        />
      </div>
      <div>
        <MyTextField
          value={state.w9}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.w9 = val))
            })
          }}
          id="w9"
          label="W-9"
        />
      </div>
      <div>
        <MyTextField
          type="number"
          value={state.numberOfEmployees}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.numberOfEmployees = val)
              )
            })
          }}
          id="numberOfEmployees"
          label="Number of Employees"
        />
      </div>
      <div>
        <MyTextField
          value={state.annualRevenue}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.annualRevenue = val))
            })
          }}
          id="annualRevenue"
          label="Annual Revenue"
        />
      </div>
    </Box>
  )
}

export default BusinessInformation
