import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Stepper from "@material-ui/core/Stepper"
import Step from "@material-ui/core/Step"
import StepLabel from "@material-ui/core/StepLabel"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import Accreditations from "./accreditations"
import BusinessContact from "./businessContact"
import BusinessInformation from "./businessInformation"
import Questions from "./questions"
import Box from "@material-ui/core/Box"
// import { useQuery } from "@apollo/react-hooks"
import { useMutation } from "@apollo/react-hooks"
import gql from "graphql-tag"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}))

function getSteps() {
  return [
    "Accreditation",
    "Business Contact",
    "Business Information",
    "Business Questions",
  ]
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return "Please enter Accreditation Information"
    case 1:
      return "Please enter Business Contact Information"
    case 2:
      return "Please enter Company Information"
    case 3:
      return "Please answer questions about your business"
    default:
      return ""
  }
}
const initAccreditation = {
  isChecked: false,
  otherClassification: "",
  agencyName: "",
  accreditationNumber: "",
  expirationDate: "",
}
const initBusinessContact = {
  businessName: "",
  contactName: "",
  contactEmail: "",
  businessPhone: "",
  cellPhone: "",
  fax: "",
  streetAddress: "",
  suiteAddress: "",
  cityAddress: "",
  stateAddress: "",
  postalCodeAddress: "",
}
const initBusinessInformation = {
  duns: "",
  certificateOfIncorporation: "",
  website: "",
  certificateOfInsurance: "",
  yearStarted: "",
  w9: "",
  numberOfEmployees: "",
  annualRevenue: "",
}
const initQuestions = {
  currentContract: { isChecked: false, detail: "" },
  productsManufactured: { isChecked: false, detail: "" },
  productsDistributed: { isChecked: false, detail: "" },
  servicesProvided: { isChecked: false, detail: "" },
  consultingProvided: { isChecked: false, detail: "" },
  constructionRelated: { isChecked: false, detail: "" },
  subcontract: { isChecked: false, detail: "" },
}

const data = {
  accreditations: {
    sbe: initAccreditation,
    wbe: initAccreditation,
    mbe: initAccreditation,
    vbe: initAccreditation,
    other: initAccreditation,
  },
  businessContact: initBusinessContact,
  businessInformation: initBusinessInformation,
  questions: initQuestions,
}
// const COMPANY_PROFILES_QUERY = gql`
//   query CompanyProfilesQuery {
//     CompanyProfiles {
//       id
//     }
//   }
// `
const CREATE_COMPANY_PROFILE_MUTATION = gql`
  mutation createCompanyProfileMutation(
    $annualRevenue: String
    $businessName: String
    $businessPhone: String
    $cellPhone: String
    $certificateOfIncorporation: String
    $certificateOfInsurance: String
    $cityAddress: String
    $constructionRelatedDetail: String
    $consultingProvidedDetail: String
    $contactEmail: String
    $contactName: String
    $currentContractDetail: String
    $duns: String
    $fax: String
    $hasConstructionRelated: Boolean
    $hasConsultingProvided: Boolean
    $hasCurrentContract: Boolean
    $hasProductsDistributed: Boolean
    $hasProductsManufactured: Boolean
    $hasServicesProvided: Boolean
    $hasSubcontract: Boolean
    $numberOfEmployees: Int
    $postalCodeAddress: String
    $productsDistributedDetail: String
    $productsManufacturedDetail: String
    $servicesProvidedDetail: String
    $stateAddress: String
    $streetAddress: String
    $subcontract: String
    $suiteAddress: String
    $w9: String
    $website: String
    $yearStarted: String
  ) {
    insert_CompanyProfiles_one(
      object: {
        annualRevenue: $annualRevenue
        businessName: $businessName
        businessPhone: $businessPhone
        cellPhone: $cellPhone
        certificateOfIncorporation: $certificateOfIncorporation
        certificateOfInsurance: $certificateOfInsurance
        cityAddress: $cityAddress
        constructionRelatedDetail: $constructionRelatedDetail
        consultingProvidedDetail: $consultingProvidedDetail
        contactEmail: $contactEmail
        contactName: $contactName
        currentContractDetail: $currentContractDetail
        duns: $duns
        fax: $fax
        hasConstructionRelated: $hasConstructionRelated
        hasConsultingProvided: $hasConsultingProvided
        hasCurrentContract: $hasCurrentContract
        hasProductsDistributed: $hasProductsDistributed
        hasProductsManufactured: $hasProductsManufactured
        hasServicesProvided: $hasServicesProvided
        hasSubcontract: $hasSubcontract
        numberOfEmployees: $numberOfEmployees
        postalCodeAddress: $postalCodeAddress
        productsDistributedDetail: $productsDistributedDetail
        productsManufacturedDetail: $productsManufacturedDetail
        servicesProvidedDetail: $servicesProvidedDetail
        stateAddress: $stateAddress
        streetAddress: $streetAddress
        subcontract: $subcontract
        suiteAddress: $suiteAddress
        w9: $w9
        website: $website
        yearStarted: $yearStarted
      }
    ) {
      id
    }
  }
`
const CREATE_ACCREDITATION_MUTATION = gql`
  mutation createAccerditationMutation(
    $accreditationNumber: String
    $active: Boolean
    $agencyName: String
    $type: String
    $companyProfileId: uuid!
    $expirationDate: String
  ) {
    insert_Accreditations_one(
      object: {
        accreditationNumber: $accreditationNumber
        active: $active
        agencyName: $agencyName
        type: $type
        companyProfileId: $companyProfileId
        expirationDate: $expirationDate
      }
    ) {
      id
    }
  }
`
const Home = () => {
  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const [createCompanyProfileMutation] = useMutation(
    CREATE_COMPANY_PROFILE_MUTATION
  )
  const [createAccreditationMutation] = useMutation(
    CREATE_ACCREDITATION_MUTATION
  )
  // const { cps, loading, error } = useQuery(COMPANY_PROFILES_QUERY, {
  //   variables: {},
  // })
  // if (loading) return <div> Loading lesson responses...</div>
  // if (error)
  //   return <div> Something deadful happened. {JSON.stringify(error)}</div>
  const steps = getSteps()
  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1)
  }
  const handleAccreditationsChange = value => {
    data.accreditations = value
  }
  const handleBusinessContactChange = value => {
    data.businessContact = value
  }
  const handleBusinessInformationChange = value => {
    data.businessInformation = value
  }
  const handleQuestionChange = value => {
    data.questions = value
  }
  const StepComponent = () => {
    switch (activeStep) {
      case 0:
        return (
          <Accreditations
            value={data.accreditations}
            onChange={handleAccreditationsChange}
          />
        )
      case 1:
        return (
          <BusinessContact
            value={data.businessContact}
            onChange={handleBusinessContactChange}
          />
        )
      case 2:
        return (
          <BusinessInformation
            value={data.businessInformation}
            onChange={handleBusinessInformationChange}
          />
        )
      case 3:
        return (
          <Questions value={data.questions} onChange={handleQuestionChange} />
        )
      default:
        return (
          <Accreditations
            value={data.accreditations}
            onChange={handleAccreditationsChange}
          />
        )
    }
  }
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1)
  }

  const handleReset = () => {
    data.accreditations = {
      sbe: initAccreditation,
      wbe: initAccreditation,
      mbe: initAccreditation,
      vbe: initAccreditation,
      other: initAccreditation,
    }
    data.businessContact = initBusinessContact
    data.businessInformation = initBusinessInformation
    data.questions = initQuestions

    setActiveStep(0)
  }
  const handleSubmitProfile = () => {
    createCompanyProfileMutation({
      variables: {
        annualRevenue: data.businessInformation.annualRevenue,
        businessName: data.businessContact.businessName,
        businessPhone: data.businessContact.businessPhone,
        cellPhone: data.businessContact.cellPhone,
        certificateOfIncorporation:
          data.businessInformation.certificateOfIncorporation,
        certificateOfInsurance: data.businessInformation.certificateOfInsurance,
        cityAddress: data.businessContact.cityAddress,
        constructionRelatedDetail: data.questions.constructionRelated.detail,
        consultingProvidedDetail: data.questions.consultingProvided.detail,
        contactEmail: data.businessContact.contactEmail,
        contactName: data.businessContact.contactName,
        currentContractDetail: data.questions.currentContract.detail,
        duns: data.businessInformation.duns,
        fax: data.businessContact.fax,
        hasConstructionRelated: data.questions.constructionRelated.isChecked,
        hasConsultingProvided: data.questions.consultingProvided.isChecked,
        hasCurrentContract: data.questions.currentContract.isChecked,
        hasProductsDistributed: data.questions.productsDistributed.isChecked,
        hasProductsManufactured: data.questions.productsManufactured.isChecked,
        hasServicesProvided: data.questions.servicesProvided.isChecked,
        hasSubcontract: data.questions.subcontract.isChecked,
        numberOfEmployees: parseInt(data.businessInformation.numberOfEmployees),
        postalCodeAddress: data.businessContact.postalCodeAddress,
        productsDistributedDetail: data.questions.productsDistributed.detail,
        productsManufacturedDetail: data.questions.productsManufactured.detail,
        servicesProvidedDetail: data.questions.servicesProvided.detail,
        stateAddress: data.businessContact.stateAddress,
        streetAddress: data.businessContact.streetAddress,
        subcontract: data.questions.subcontract.detail,
        suiteAddress: data.businessContact.suiteAddress,
        w9: data.businessInformation.w9,
        website: data.businessInformation.website,
        yearStarted: data.businessInformation.yearStarted,
      },
      optimisticResponse: false,
    })
      .then(response => {
        const companyProfileId = response.data.insert_CompanyProfiles_one.id
        createAccreditationMutation({
          variables: {
            accreditationNumber: data.accreditations.sbe.accreditationNumber,
            expirationDate: data.accreditations.sbe.expirationDate,
            active: data.accreditations.sbe.active,
            agencyName: data.accreditations.sbe.agencyName,
            type: "sbe",
            companyProfileId: companyProfileId,
          },
        })
        createAccreditationMutation({
          variables: {
            accreditationNumber: data.accreditations.wbe.accreditationNumber,
            expirationDate: data.accreditations.wbe.expirationDate,
            active: data.accreditations.wbe.active,
            agencyName: data.accreditations.wbe.agencyName,
            type: "wbe",
            companyProfileId: companyProfileId,
          },
        })
        createAccreditationMutation({
          variables: {
            accreditationNumber: data.accreditations.mbe.accreditationNumber,
            expirationDate: data.accreditations.mbe.expirationDate,
            active: data.accreditations.mbe.active,
            agencyName: data.accreditations.mbe.agencyName,
            type: "mbe",
            companyProfileId: companyProfileId,
          },
        })
        createAccreditationMutation({
          variables: {
            accreditationNumber: data.accreditations.vbe.accreditationNumber,
            expirationDate: data.accreditations.vbe.expirationDate,
            active: data.accreditations.vbe.active,
            agencyName: data.accreditations.vbe.agencyName,
            type: "vbe",
            companyProfileId: companyProfileId,
          },
        })
        createAccreditationMutation({
          variables: {
            accreditationNumber: data.accreditations.other.accreditationNumber,
            expirationDate: data.accreditations.other.expirationDate,
            active: data.accreditations.other.active,
            agencyName: data.accreditations.other.agencyName,
            type: "other",
            companyProfileId: companyProfileId,
          },
        })
      })
      .catch(err => {
        console.log("this is not good", err)
      })
  }

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed
            </Typography>
            <Box m={3}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSubmitProfile}
              >
                Submit Company Profile
              </Button>
            </Box>
            <Box m={3}>
              <Button variant="contained" onClick={handleReset}>
                Reset
              </Button>
            </Box>
          </div>
        ) : (
          <Box mt={3} ml={10} mr={10}>
            <Box mb={3}>
              <Typography variant="h4" className={classes.instructions}>
                {getStepContent(activeStep)}
              </Typography>
            </Box>
            <StepComponent />
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Back
              </Button>
              <Button variant="contained" color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? "Finish" : "Next"}
              </Button>
            </div>
          </Box>
        )}
      </div>
    </div>
  )
}

export default Home
