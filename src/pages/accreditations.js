import React from "react"
import Typography from "@material-ui/core/Typography"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import TextField from "@material-ui/core/TextField"
import Collapse from "@material-ui/core/Collapse"
import Paper from "@material-ui/core/Paper"
import Box from "@material-ui/core/Box"

const Accreditations = ({ value, onChange }) => {
  const data = value
  const Classification = ({ type, label, value, onChange }) => {
    const [isChecked, setIsChecked] = React.useState(value.isChecked)
    const [otherClassification, setOtherClassification] = React.useState(
      value.setOtherClassification
    )
    const [agencyName, setAgencyName] = React.useState(value.agencyName)
    const [accreditationNumber, setAccreditationNumber] = React.useState(
      value.accreditationNumber
    )
    const [expirationDate, setExpirationDate] = React.useState(
      value.expirationDate
    )
    const [
      otherClassificationInvalid,
      setOtherClassificationInvalid,
    ] = React.useState()
    const [agencyNameInvalid, setAgencyNameInvalid] = React.useState()
    const [numberInvalid, setNumberInvalid] = React.useState()
    const [expirationInvalid, setExpirationInvalid] = React.useState()

    React.useEffect(() => {
      if (onChange)
        onChange({
          type,
          isChecked,
          otherClassification,
          agencyName,
          accreditationNumber,
          expirationDate,
        })
    }, [
      type,
      isChecked,
      otherClassification,
      agencyName,
      accreditationNumber,
      expirationDate,
      onChange,
    ])

    return (
      <Paper>
        <Box m={3}>
          <FormControlLabel
            control={
              <Checkbox
                checked={isChecked}
                onChange={() => {
                  setIsChecked(prev => !prev)
                }}
                name={`${type}-checked`}
                color="primary"
              />
            }
            label={label}
          />

          <Collapse in={isChecked}>
            <Box mb={3}>
              {"other" === type && (
                <div>
                  <TextField
                    required
                    fullWidth
                    error={otherClassificationInvalid}
                    id={`${type}-clasification`}
                    label="Accreditation"
                    value={otherClassification}
                    onChange={e =>
                      setOtherClassification(prev => {
                        return e.target.value
                      })
                    }
                    onBlur={() =>
                      setOtherClassificationInvalid(
                        otherClassification.length <= 0
                      )
                    }
                  />
                </div>
              )}

              <div>
                <TextField
                  required
                  fullWidth
                  error={agencyNameInvalid}
                  id={`${type}-agency`}
                  name={`${type}-agency`}
                  label="Accrediting Agency Name"
                  value={agencyName}
                  onChange={e => setAgencyName(e.target.value)}
                  onBlur={() => setAgencyNameInvalid(agencyName.length <= 0)}
                />
              </div>
              <div>
                <TextField
                  required
                  fullWidth
                  error={numberInvalid}
                  id={`${type}-number`}
                  label="Accreditation Number"
                  value={accreditationNumber}
                  onChange={e => setAccreditationNumber(e.target.value)}
                  onBlur={() =>
                    setNumberInvalid(accreditationNumber.length <= 0)
                  }
                />
              </div>
              <div>
                <TextField
                  required
                  id={`${type}-expirationDate`}
                  label="Expiration Date"
                  type="date"
                  error={expirationInvalid}
                  value={expirationDate}
                  onChange={e => setExpirationDate(e.target.value)}
                  onBlur={() =>
                    setExpirationInvalid(expirationDate.length <= 0)
                  }
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
            </Box>
          </Collapse>
        </Box>
      </Paper>
    )
  }

  const handelSbeChange = sbeData => {
    data.sbe = sbeData
    onChange(data)
  }
  const handelWbeChange = wbeData => {
    data.wbe = wbeData
    onChange(data)
  }
  const handelMbeChange = mbeData => {
    data.mbe = mbeData
    onChange(data)
  }
  const handelVbeChange = vbeData => {
    data.vbe = vbeData
    onChange(data)
  }
  const handelOtherChange = otherData => {
    data.other = otherData
    onChange(data)
  }
  if (!value) return <p>loading...</p>
  return (
    <>
      <Typography>
        Please select all the vendor classifications your company fits under.
      </Typography>
      <Classification
        type="sbe"
        value={value.sbe}
        label="SBE - Small Business Enterprise"
        onChange={handelSbeChange}
      />
      <Classification
        type="wbe"
        value={value.wbe}
        label="WBE - Women Business Enterprise"
        onChange={handelWbeChange}
      />
      <Classification
        type="mbe"
        value={value.mbe}
        label="MBE - Minority Business Enterprise"
        onChange={handelMbeChange}
      />
      <Classification
        type="vbe"
        value={value.vbe}
        label="VBE - Vetran Business Enterprise"
        onChange={handelVbeChange}
      />
      <Classification
        type="other"
        value={value.other}
        label="Other"
        onChange={handelOtherChange}
      />
    </>
  )
}
export default Accreditations
