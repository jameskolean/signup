import React from "react"
import Typography from "@material-ui/core/Typography"
import TextField from "@material-ui/core/TextField"
import Collapse from "@material-ui/core/Collapse"
import produce from "immer"
import Box from "@material-ui/core/Box"
import Paper from "@material-ui/core/Paper"

import { withStyles, createStyles } from "@material-ui/core/styles"
import Switch from "@material-ui/core/Switch"
import Grid from "@material-ui/core/Grid"

const AntSwitch = withStyles(theme =>
  createStyles({
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: "flex",
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      "&$checked": {
        transform: "translateX(12px)",
        color: theme.palette.common.white,
        "& + $track": {
          opacity: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 12,
      height: 12,
      boxShadow: "none",
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
  })
)(Switch)

const MyTextField = ({ id, label, value, onChange, type, required }) => {
  const [error, setError] = React.useState()
  const fieldType = type ? type : "text"
  const fieldRequired = required === undefined ? true : required
  return (
    <div>
      <TextField
        required={fieldRequired}
        type={fieldType}
        fullWidth
        error={error}
        id={id}
        label={label}
        value={value}
        onChange={e => onChange(e.target.value)}
        onBlur={e => setError(e.target.value <= 0)}
      />
    </div>
  )
}
const Question = ({ id, label, sublabel, value, onChange, required }) => {
  const [state, setState] = React.useState(value)

  React.useEffect(() => {
    if (onChange) onChange(state)
  }, [state, onChange])

  return (
    <Paper>
      <Box m={3} pt={3}>
        <Typography>{label}</Typography>
        <Typography component="div">
          <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>No</Grid>
            <Grid item>
              <AntSwitch
                checked={state.isChecked}
                id={`${id}-checked`}
                onChange={e => {
                  setState(prev =>
                    produce(
                      prev,
                      draft => void (draft.isChecked = e.target.checked)
                    )
                  )
                }}
              />
            </Grid>
            <Grid item>Yes</Grid>
          </Grid>
        </Typography>
        <Collapse in={state.isChecked}>
          <Box mb={3}>
            <MyTextField
              value={state.detail}
              onChange={val => {
                setState(prev => {
                  return produce(prev, draft => void (draft.detail = val))
                })
              }}
              id={`${id}-detail`}
              label={sublabel}
              required={required}
            />
          </Box>
        </Collapse>
      </Box>
    </Paper>
  )
}

const Questions = ({ value, onChange }) => {
  const [state, setState] = React.useState(value)
  React.useEffect(() => {
    if (onChange) onChange(state)
  }, [state, onChange])
  if (!value) return <p>loading...</p>
  return (
    <>
      <div>
        <Question
          id="currentContract"
          value={state.currentContract}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.currentContract = val))
            })
          }}
          label="Are you doing business with any Orlando Health hospitals or Offices?"
          sublabel="Please specify locations."
        />
      </div>
      <div>
        <Question
          id="products"
          value={state.productsManufactured}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.productsManufactured = val)
              )
            })
          }}
          label="Do you make or manufacture a product?"
          sublabel="Please list products."
        />
      </div>
      <div>
        <Question
          id="productsDistributed"
          value={state.productsDistributed}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.productsDistributed = val)
              )
            })
          }}
          label="Do you distrubute product?"
          sublabel="Please list the brands and products."
        />
      </div>
      <div>
        <Question
          id="servicesProvided"
          value={state.servicesProvided}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.servicesProvided = val))
            })
          }}
          label="Do you provide service?"
          sublabel="Please list the services you provide."
        />
      </div>
      <div>
        <Question
          id="consultingProvided"
          value={state.consultingProvided}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.consultingProvided = val)
              )
            })
          }}
          label="Do you provide consulting?"
          sublabel="Please list categories of your expertise."
        />
      </div>
      <div>
        <Question
          id="constructionRelated"
          value={state.constructionRelated}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.constructionRelated = val)
              )
            })
          }}
          label="Is your service or product construction related?"
          sublabel="Details."
          required={false}
        />
      </div>
      <div>
        <Question
          id="subcontract"
          value={state.subcontract}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.subcontract = val))
            })
          }}
          label="Do you sub-contract labor?"
          sublabel="Details."
          required={false}
        />
      </div>
    </>
  )
}

export default Questions
