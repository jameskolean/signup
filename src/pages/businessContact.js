import React from "react"
import TextField from "@material-ui/core/TextField"
import produce from "immer"
import Box from "@material-ui/core/Box"

const MyTextField = ({ id, label, value, onChange, type }) => {
  const [error, setError] = React.useState()
  const fieldType = type ? type : "text"
  return (
    <div>
      <TextField
        required
        type={fieldType}
        fullWidth
        error={error}
        id={id}
        label={label}
        value={value}
        onChange={e => onChange(e.target.value)}
        onBlur={e => setError(e.target.value <= 0)}
      />
    </div>
  )
}

const BusinessContact = ({ value, onChange }) => {
  const [state, setState] = React.useState(value)
  React.useEffect(() => {
    if (onChange) onChange(state)
  }, [state, onChange])
  if (!value) return <p>loading...</p>

  return (
    <Box mb={3}>
      <div>
        <MyTextField
          value={state.businessName}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.businessName = val))
            })
          }}
          id="businessName"
          label="Business Name"
        />
      </div>
      <div>
        <MyTextField
          value={state.contactName}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.contactName = val))
            })
          }}
          id="contactName"
          label="Contact Name"
        />
      </div>
      <div>
        <MyTextField
          type="email"
          value={state.contactEmail}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.contactEmail = val))
            })
          }}
          id="contactEmail"
          label="Contact Email"
        />
      </div>
      <div>
        <MyTextField
          value={state.businessPhone}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.businessPhone = val))
            })
          }}
          id="businessPhone"
          label="Business Phone"
        />
      </div>
      <div>
        <MyTextField
          value={state.cellPhone}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.cellPhone = val))
            })
          }}
          id="cellPhone"
          label="Cell"
        />
      </div>
      <div>
        <MyTextField
          value={state.fax}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.fax = val))
            })
          }}
          id="fax"
          label="Fax"
        />
      </div>
      <div>
        <MyTextField
          value={state.streetAddress}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.streetAddress = val))
            })
          }}
          id="streetAddress"
          label="Business Street Address"
        />
      </div>
      <div>
        <MyTextField
          value={state.suiteAddress}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.suiteAddress = val))
            })
          }}
          id="suiteAddress"
          label="Suite/Mail point"
        />
      </div>
      <div>
        <MyTextField
          value={state.stateAddress}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.stateAddress = val))
            })
          }}
          id="stateAddress"
          label="State"
        />
      </div>
      <div>
        <MyTextField
          value={state.cityAddress}
          onChange={val => {
            setState(prev => {
              return produce(prev, draft => void (draft.cityAddress = val))
            })
          }}
          id="cityAddress"
          label="City"
        />
      </div>
      <div>
        <MyTextField
          value={state.postalCodeAddress}
          onChange={val => {
            setState(prev => {
              return produce(
                prev,
                draft => void (draft.postalCodeAddress = val)
              )
            })
          }}
          id="postalCodeAddress"
          label="ZIP"
        />
      </div>
    </Box>
  )
}
export default BusinessContact
