import React from "react"
import { ApolloProvider } from "@apollo/react-hooks"
import ApolloClient from "apollo-boost"
import fetch from "isomorphic-fetch"

const client = new ApolloClient({
  uri: process.env.GATSBY_HASURA_URL,
  fetch,
})

const ApolloRootElementWrapper = ({ element }) => {
  return <ApolloProvider client={client}>{element}</ApolloProvider>
}

export default ApolloRootElementWrapper
