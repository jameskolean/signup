/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */
import React from "react"
import ApolloRootElementWrapper from "./src/utils/apollo"

export const wrapRootElement = ({ element }) => (
  <ApolloRootElementWrapper element={element} />
)
